<?php
/**
 * Client Post Type functionality
 *
 * @since   0.1.0
 * @package Sallys_Atomic_Post_Types
 */

/**
 * Registers the `client` post type.
 */
function client_init() {
	register_post_type(
		'client', array(
			'labels'                => array(
				'name'                  => __( 'Clients', 'sallys-atomic-blocks' ),
				'singular_name'         => __( 'Client', 'sallys-atomic-blocks' ),
				'all_items'             => __( 'All Clients', 'sallys-atomic-blocks' ),
				'archives'              => __( 'Client Archives', 'sallys-atomic-blocks' ),
				'attributes'            => __( 'Client Attributes', 'sallys-atomic-blocks' ),
				'insert_into_item'      => __( 'Insert into client', 'sallys-atomic-blocks' ),
				'uploaded_to_this_item' => __( 'Uploaded to this client', 'sallys-atomic-blocks' ),
				'featured_image'        => _x( 'Featured Image', 'client', 'sallys-atomic-blocks' ),
				'set_featured_image'    => _x( 'Set featured image', 'client', 'sallys-atomic-blocks' ),
				'remove_featured_image' => _x( 'Remove featured image', 'client', 'sallys-atomic-blocks' ),
				'use_featured_image'    => _x( 'Use as featured image', 'client', 'sallys-atomic-blocks' ),
				'filter_items_list'     => __( 'Filter clients list', 'sallys-atomic-blocks' ),
				'items_list_navigation' => __( 'Clients list navigation', 'sallys-atomic-blocks' ),
				'items_list'            => __( 'Clients list', 'sallys-atomic-blocks' ),
				'new_item'              => __( 'New Client', 'sallys-atomic-blocks' ),
				'add_new'               => __( 'Add New', 'sallys-atomic-blocks' ),
				'add_new_item'          => __( 'Add New Client', 'sallys-atomic-blocks' ),
				'edit_item'             => __( 'Edit Client', 'sallys-atomic-blocks' ),
				'view_item'             => __( 'View Client', 'sallys-atomic-blocks' ),
				'view_items'            => __( 'View Clients', 'sallys-atomic-blocks' ),
				'search_items'          => __( 'Search clients', 'sallys-atomic-blocks' ),
				'not_found'             => __( 'No clients found', 'sallys-atomic-blocks' ),
				'not_found_in_trash'    => __( 'No clients found in trash', 'sallys-atomic-blocks' ),
				'parent_item_colon'     => __( 'Parent Client:', 'sallys-atomic-blocks' ),
				'menu_name'             => __( 'Clients', 'sallys-atomic-blocks' ),
			),
			'public'                => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_nav_menus'     => true,
			'supports'              => array( 'page-attributes', 'title', 'editor', 'author', 'thumbnail' ),
			'has_archive'           => true,
			'rewrite'               => true,
			'query_var'             => true,
			'menu_icon'             => 'dashicons-businessman',
			'show_in_rest'          => true,
			'rest_base'             => 'client',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
			'taxonomies'            => array( 'category' ),
		)
	);

}
add_action( 'init', 'client_init' );

/**
 * Sets the post updated messages for the `client` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `client` post type.
 */
function client_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['client'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Client updated. <a target="_blank" href="%s">View client</a>', 'sallys-atomic-blocks' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'sallys-atomic-blocks' ),
		3  => __( 'Custom field deleted.', 'sallys-atomic-blocks' ),
		4  => __( 'Client updated.', 'sallys-atomic-blocks' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Client restored to revision from %s', 'sallys-atomic-blocks' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Client published. <a href="%s">View client</a>', 'sallys-atomic-blocks' ), esc_url( $permalink ) ),
		7  => __( 'Client saved.', 'sallys-atomic-blocks' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Client submitted. <a target="_blank" href="%s">Preview client</a>', 'sallys-atomic-blocks' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf(
			__( 'Client scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview client</a>', 'sallys-atomic-blocks' ),
			date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink )
		),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Client draft updated. <a target="_blank" href="%s">Preview client</a>', 'sallys-atomic-blocks' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'client_updated_messages' );
