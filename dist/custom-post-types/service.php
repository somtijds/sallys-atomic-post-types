<?php
/**
 * Service Post Type functionality
 *
 * @since   0.1.0
 * @package Sallys_Atomic_Post_Types
 */

/**
 * Registers the `service` post type.
 */
function service_init() {
	register_post_type(
		'service', array(
			'labels'                => array(
				'name'                  => __( 'Services', 'sallys-atomic-blocks' ),
				'singular_name'         => __( 'Service', 'sallys-atomic-blocks' ),
				'all_items'             => __( 'All Services', 'sallys-atomic-blocks' ),
				'archives'              => __( 'Service Archives', 'sallys-atomic-blocks' ),
				'attributes'            => __( 'Service Attributes', 'sallys-atomic-blocks' ),
				'insert_into_item'      => __( 'Insert into service', 'sallys-atomic-blocks' ),
				'uploaded_to_this_item' => __( 'Uploaded to this service', 'sallys-atomic-blocks' ),
				'featured_image'        => _x( 'Featured Image', 'service', 'sallys-atomic-blocks' ),
				'set_featured_image'    => _x( 'Set featured image', 'service', 'sallys-atomic-blocks' ),
				'remove_featured_image' => _x( 'Remove featured image', 'service', 'sallys-atomic-blocks' ),
				'use_featured_image'    => _x( 'Use as featured image', 'service', 'sallys-atomic-blocks' ),
				'filter_items_list'     => __( 'Filter services list', 'sallys-atomic-blocks' ),
				'items_list_navigation' => __( 'Services list navigation', 'sallys-atomic-blocks' ),
				'items_list'            => __( 'Services list', 'sallys-atomic-blocks' ),
				'new_item'              => __( 'New Service', 'sallys-atomic-blocks' ),
				'add_new'               => __( 'Add New', 'sallys-atomic-blocks' ),
				'add_new_item'          => __( 'Add New Service', 'sallys-atomic-blocks' ),
				'edit_item'             => __( 'Edit Service', 'sallys-atomic-blocks' ),
				'view_item'             => __( 'View Service', 'sallys-atomic-blocks' ),
				'view_items'            => __( 'View Services', 'sallys-atomic-blocks' ),
				'search_items'          => __( 'Search services', 'sallys-atomic-blocks' ),
				'not_found'             => __( 'No services found', 'sallys-atomic-blocks' ),
				'not_found_in_trash'    => __( 'No services found in trash', 'sallys-atomic-blocks' ),
				'parent_item_colon'     => __( 'Parent Service:', 'sallys-atomic-blocks' ),
				'menu_name'             => __( 'Services', 'sallys-atomic-blocks' ),
			),
			'public'                => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_nav_menus'     => true,
			'supports'              => array( 'page-attributes', 'title', 'editor', 'author', 'thumbnail' ),
			'has_archive'           => true,
			'rewrite'               => true,
			'query_var'             => true,
			'menu_icon'             => 'dashicons-megaphone',
			'show_in_rest'          => true,
			'rest_base'             => 'service',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
			'taxonomies'            => array( 'category' ),
		)
	);

}
add_action( 'init', 'service_init' );


/**
 * Sets the post updated messages for the `service` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `service` post type.
 */
function service_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['service'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Service updated. <a target="_blank" href="%s">View service</a>', 'sallys-atomic-blocks' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'sallys-atomic-blocks' ),
		3  => __( 'Custom field deleted.', 'sallys-atomic-blocks' ),
		4  => __( 'Service updated.', 'sallys-atomic-blocks' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Service restored to revision from %s', 'sallys-atomic-blocks' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Service published. <a href="%s">View service</a>', 'sallys-atomic-blocks' ), esc_url( $permalink ) ),
		7  => __( 'Service saved.', 'sallys-atomic-blocks' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Service submitted. <a target="_blank" href="%s">Preview service</a>', 'sallys-atomic-blocks' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf(
			__( 'Service scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview service</a>', 'sallys-atomic-blocks' ),
			date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink )
		),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Service draft updated. <a target="_blank" href="%s">Preview service</a>', 'sallys-atomic-blocks' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'service_updated_messages' );
