<?php
/**
 * Plugin Name:     Sally's Atomic Post Types and Blocks
 * Plugin URI:      https://bitbucket.org/somtijds/sallys-atomic-post-types-and-blocks
 * Description:     Helper plugin for Sally's Atomic Blocks to create custom post types and blocks for services and clients.
 * Author:          Willem Prins | SOMTIJDS <willem@somtijds.de>
 * Author URI:      https://www.somtijds.de
 * Text Domain:     sallys-atomic-post-types-and-blocks
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Sallys_Atomic_Post_Types_And_Blocks
 */

// Your code starts here.
require_once plugin_dir_path( __FILE__ ) . 'dist/custom-post-types/client.php';
require_once plugin_dir_path( __FILE__ ) . 'dist/custom-post-types/service.php';

